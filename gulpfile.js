const {
    task,
    series,
    parallel,
    src,
    watch,
    dest
} = require('gulp');

let sass = require('gulp-sass'),
    sourceMaps = require('gulp-sourcemaps'),
    urlAdjust = require('gulp-css-url-adjuster'),
    BrowserSync = require('browser-sync').create(),
    cleanCss = require('gulp-clean-css'),
    minify = require('gulp-minify'),
    rename = require('gulp-rename'),
    reload = BrowserSync.reload,
    imagemin = require('gulp-imagemin');

// Paths
let css_path = 'public/css',
    js_path = 'public/js',
    fonts_path  = 'public/fonts' ,
    img_path = 'public/images';


// Compile/process SCSS 
function compile_sass() {
    return src([
        'src/scss/*.scss',
        
    ])
        .pipe(sourceMaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(urlAdjust({
            replace: ['./fonts/slick', '../fonts/slick']
        }))
        .pipe(cleanCss())  
        .pipe(sourceMaps.write())
        .pipe(rename('style.min.css'))
        .pipe(dest(css_path))
        .pipe(BrowserSync.stream());
}

//Compile JS
function compile_js() {
    return src([
        'src/js/*.js',
        'node_modules/jquery/dist/jquery.min.js',
        // 'node_modules/popper.js/dist/popper.min.js',
        'node_modules/slick-carousel/slick/slick.min.js'
    ])
    .pipe(dest(js_path))
    .pipe(BrowserSync.stream());
}

// Processes images
function images() {
    return src(['src/images/*'])
        // .pipe(imagemin())
        .pipe(dest(img_path));
}


// Fonts
function webfonts() {
    return src(['node_modules/@fortawesome/fontawesome-free/webfonts/*'])
        .pipe(dest(fonts_path + '/webfonts'));
}

function pageFonts() {
    return src(['src/fonts/*'])
        .pipe(dest(fonts_path));
}

function slickFonts() {
    return src([
        'node_modules/slick-carousel/slick/fonts/*'
    ])
    .pipe(dest(fonts_path + '/slick/'));
}


// Watching
function watch_files() {
    BrowserSync.init({
        server: {
            baseDir: './public'
        },
        open: "tunnel"
    });

    // Watch SCSS files and Reload
    watch('src/scss/**/*.scss', compile_sass);
    
    // Watch JS files and Reload
    watch('src/js/*.js').on('change', compile_js);

    // Watch HTML files and Reload
    watch(['./public/*.html']).on('change', reload);
}

// Default task
task('default', 
    series(
        compile_sass,
        compile_js,
        webfonts,
        pageFonts,
        slickFonts,
        images,
        watch_files
    )
);
