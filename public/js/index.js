$(document).ready(function () {
    $('.slider-1').slick({
        slidesToShow: 3,
        infinite: true,
        dots: true,
        nextArrow: '<i class="fas fa-chevron-right custom-next"></i>',
        prevArrow: '<i class="fas fa-chevron-left custom-prev"></i>',
        responsive: [{
            breakpoint: 1024,
            settings: {
                slidesToShow: 2,
                }
        },{
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                }
        }]
    });
});